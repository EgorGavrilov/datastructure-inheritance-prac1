﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance.DataStructure 
{
    class Category : IComparable
    {
        private string category;
        private MessageType type;
        private MessageTopic topic;

        public Category(string category, MessageType type, MessageTopic topic)
        {
            this.category = category;
            this.type = type;
            this.topic = topic;
        }

        public int CompareTo(object obj)
        {
            var c = obj as Category;
            var caterogyCompare = category.CompareTo(c.category);
            if (caterogyCompare != 0) return caterogyCompare;
            else
            {
                if (type < c.type) return -1;
                if (type > c.type) return 1;
                if (topic < c.topic) return -1;
                if (topic > c.topic) return 1;
            }
            return 0;
        }

        public override bool Equals(object obj)
        {
            var c = obj as Category;
            return (this.category == c.category && type == c.type && topic == c.topic);
        }

        public static bool operator >(Category category1, Category category2)
        {
            return (category1.CompareTo(category2) == 1);
        }

        public static bool operator <(Category category1, Category category2)
        {
            return (category1.CompareTo(category2) == -1);
        }

        public static bool operator >=(Category category1, Category category2)
        {
            return (category1.CompareTo(category2) != -1);
        }

        public static bool operator <=(Category category1, Category category2)
        {
            return (category1.CompareTo(category2) != 1);
        }

        public static bool operator ==(Category category1, Category category2)
        {
            return category1.Equals(category2);
        }

        public static bool operator !=(Category category1, Category category2)
        {
            return !category1.Equals(category2);
        }

        public override string ToString()
        {
            return $"{category}.{type}.{topic}";
        }

        public override int GetHashCode()
        {
            return category.ToArray().Select(x => (int) x).ToArray().Sum() + (int)type + (int)topic;
        }
    }
}
